#!/bin/sh

# Mysql dump variables
FILE={filename}.sql.`date +"%Y%m%d"`
DBSERVER={IP}
DATABASE={DB NAME}
USER={username}
PASS={password}

#in case you run this more than once a day, remove the previous version of the file
#unalias rm     2> /dev/null
#rm ${FILE}     2> /dev/null
#rm ${FILE}.gz  2> /dev/null

# Mysql Dump procedure

# if the dump is in server on seperate host
mysqldump --opt --protocol=TCP --user=${USER} --password=${PASS} --host=${DBSERVER} ${DATABASE} > ${FILE}

# if the dump is in local host
#mysqldump --opt --user=${USER} --password=${PASS} ${DATABASE} > ${FILE}

# tar the mysql database dump file
tar -xcvf $FILE $FILE.tgz
 
# Final Output
echo "${FILE}.tgz was created:"
ls -l ${FILE}.tgz